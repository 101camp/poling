## Full Time Activities
> total commit: **103**

![](https://ipic.zoomquiet.top/201012-t5pol.jpg)

### Top 5 Commit push

```

junstudy  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 43.00
oneeyeQ   : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 27.00
jingxuanz : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 25.00
ZoomQuiet : 🔥🔥🔥🔥🔥 8.00 
wanghua101:  0.00 


```

### Top 5 commit-comments words

```

ZoomQuiet    : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 2771.00
oneeyeQ      : 🔥🔥🔥 300.00
izhangshiying:  32.00
wanghua101   :  0.00 
jingxuanz    :  0.00 


```

### Top 5 Issue times

```

ZoomQuiet : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 27.00
oneeyeQ   : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 14.00
jingxuanz : 🔥🔥🔥🔥🔥🔥🔥🔥🔥 9.00 
junstudy  : 🔥🔥🔥🔥🔥🔥🔥 7.00 
wanghua101:  0.00 


```

### Top 5 Issue-comments words

```

ZoomQuiet : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 14718.00
oneeyeQ   : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 5509.00
jingxuanz : 🔥🔥🔥 1814.00
junstudy  : 🔥🔥🔥 1804.00
wanghua101:  103.00


```

## Last 2 Weeks Activities:
> total commit: **37**

![](https://ipic.zoomquiet.top/201012-w2act.jpg)


### Top 5 Commit push / half a month

```

junstudy  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 13.00
oneeyeQ   : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 11.00
jingxuanz : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 7.00 
ZoomQuiet : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 6.00 
wanghua101:  0.00 


```

### Top 5 commit-comments words / half a month

```

ZoomQuiet    : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 2669.00
oneeyeQ      : 🔥🔥🔥 300.00
izhangshiying:  32.00
wanghua101   :  0.00 
jingxuanz    :  0.00 


```

### Top 5 Issue times / half a month

```

ZoomQuiet : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 13.00
oneeyeQ   : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 7.00 
jingxuanz : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 5.00 
junstudy  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 5.00 
wanghua101:  0.00 


```

### Top 5 Issue-comments words / half a month

```

ZoomQuiet    : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 8455.00
oneeyeQ      : 🔥🔥🔥🔥🔥 1552.00
jingxuanz    : 🔥🔥🔥 1081.00
junstudy     : 🔥🔥 808.00
izhangshiying:  37.00


```


> gen. (as 
GL_PoL v.200801.1942 by:ZoomQuiet
)


