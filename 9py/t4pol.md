## Full Time Activities
> total commit: **177**


### Top 5 Commit push

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 24.00
peace101   : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 14.00
rabbit-jump: 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 12.00
machenxu101: 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 11.00
nydaym     : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 10.00


```

### Top 5 commit-comments words

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 10065.00
Spehhhhh   : 🔥🔥 723.00
huxmu      :  268.00
sunhl      :  230.00
rabbit-jump:  169.00


```

### Top 5 Issue times

```

ZoomQuiet: 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 115.00
sunhl    : 🔥🔥🔥🔥🔥🔥🔥🔥 36.00
huxmu    : 🔥🔥🔥 15.00
mymbbzm  : 🔥🔥 12.00
gmsqq    : 🔥🔥 10.00


```

### Top 5 Issue-comments words

```

ZoomQuiet   : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 88509.00
mymbbzm     : 🔥🔥 8633.00
longjianfeng: 🔥🔥 8060.00
huxmu       : 🔥 5853.00
Spehhhhh    :  2965.00


```

## Last 2 Weeks Activities:
> total commit: **21**


### Top 5 Commit push / half a month

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 6.00 
dananrou   : 🔥🔥🔥🔥🔥🔥🔥🔥🔥 2.00 
mymbbzm    : 🔥🔥🔥🔥🔥🔥🔥🔥🔥 2.00 
machenxu101: 🔥🔥🔥🔥🔥🔥🔥🔥🔥 2.00 
rabbit-jump: 🔥🔥🔥🔥🔥🔥🔥🔥🔥 2.00 


```

### Top 5 commit-comments words / half a month

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 4136.00
huxmu      : 🔥 268.00
rabbit-jump:  124.00
dananrou   :  28.00
machenxu101:  18.00


```

### Top 5 Issue times / half a month

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 39.00
mymbbzm    : 🔥🔥 4.00 
rabbit-jump: 🔥🔥 4.00 
machenxu101: 🔥🔥 3.00 
peace101   : 🔥 2.00 


```

### Top 5 Issue-comments words / half a month

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 19063.00
jenny101   : 🔥 888.00
rabbit-jump: 🔥 842.00
machenxu101:  672.00
mymbbzm    :  582.00


```


> gen. (as 
GL_PoL v.200729.1142 by:ZoomQuiet
)



