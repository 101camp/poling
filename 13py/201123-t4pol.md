## Full Time Activities
> total commit: **35**

![](https://ipic.zoomquiet.top/201123-t5pol.jpg)

### Top 5 Commit push

```

vieta      : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 20.00
ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥 7.00 
wonius     : 🔥🔥🔥🔥🔥🔥🔥 5.00 
luyanspring: 🔥🔥🔥🔥 3.00 
Weida      :  0.00 


```

### Top 5 commit-comments words

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 314.00
vieta      :  0.00 
wonius     :  0.00 
luyanspring:  0.00 
Weida      :  0.00 


```

### Top 5 Issue times

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 30.00
vieta      : 🔥🔥🔥🔥🔥🔥🔥 8.00 
wonius     : 🔥🔥🔥🔥🔥🔥 7.00 
luyanspring:  0.00 
Weida      :  0.00 


```

### Top 5 Issue-comments words

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 7780.00
vieta      : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 4880.00
wonius     : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 3077.00
luyanspring:  0.00 
Weida      :  0.00 


```

## Last 2 Weeks Activities:
> total commit: **2**

![](https://ipic.zoomquiet.top/201123-w2act.jpg)


### Top 5 Commit push / half a month

```

wonius     : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 1.00 
ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 1.00 
vieta      :  0.00 
luyanspring:  0.00 
Weida      :  0.00 


```

### Top 5 commit-comments words / half a month

```


```

### Top 5 Issue times / half a month

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 6.00 
wonius     : 🔥🔥🔥🔥🔥🔥🔥🔥🔥 2.00 
vieta      :  0.00 
luyanspring:  0.00 
Weida      :  0.00 


```

### Top 5 Issue-comments words / half a month

```

ZoomQuiet  : 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 1368.00
wonius     : 🔥🔥🔥🔥🔥🔥🔥🔥🔥 461.00
vieta      :  0.00 
luyanspring:  0.00 
Weida      :  0.00 


```


> gen. (as 
GL_PoL v.200801.1942 by:ZoomQuiet
)



